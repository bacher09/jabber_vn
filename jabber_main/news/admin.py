from django.contrib import admin
from news.models import News

class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_datetime', 'updated_datetime', 'slug', 'published')
    list_filter = ('published',)
    date_hierarchy = 'created_datetime'
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ('title', 'slug')
    #list_editable =  ('published',)
    #filter_horizontal = ('published',k)
    

admin.site.register(News, NewsAdmin)
