from django.db import models
from taggit.managers import TaggableManager

class AbstractDateTimeModel(models.Model):
    created_datetime = models.DateTimeField(auto_now_add = True)
    updated_datetime = models.DateTimeField(auto_now = True)

    class Meta:
        abstract = True

class News(AbstractDateTimeModel):
    title = models.CharField(max_length = 120)
    body = models.TextField()
    slug = models.SlugField()
    published = models.BooleanField(default = False)
    tags = TaggableManager(blank = True)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('news_item', (), {
                'slug': self.slug,
            })

    @models.permalink
    def get_absolute_url_pk(self):
        return ('news_item', (), {
                'pk': self.pk,
            })
