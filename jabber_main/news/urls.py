from django.conf.urls import patterns, include, url
from news.views import NewsRssFeed

urlpatterns = patterns('news.views',
    url(r'^(?:tag/(?P<tag>.*)/)?$', 'news_view', name = 'news'),
    url(r'^rss/$', NewsRssFeed(), name = 'news_rss'),
    url(r'^(?P<pk>\d+)/$', 'news_item_view', name = 'news_item'),
    url(r'^(?P<slug>[^/]+)/$', 'news_item_view', name = 'news_item'),
)
