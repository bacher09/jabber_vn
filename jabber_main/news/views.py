from django.views.generic import ListView, DetailView
from django.contrib.sitemaps import Sitemap
from django.contrib.syndication.views import Feed
from news.models import News

class NewsView(ListView):
    context_object_name = 'news'
    template_name = 'news.html'
    paginate_by = 10
    queryset = News.objects.filter(published = True). \
        order_by('-created_datetime')

    def get_queryset(self):
        q = super(NewsView, self).get_queryset()
        tag = self.kwargs.get('tag')
        if tag is not None:
            q = q.filter(tags__name = tag)
        return q
        

news_view = NewsView.as_view()

class NewsItemView(DetailView):
    context_object_name = 'news_item'
    template_name = 'news_item.html'
    queryset = News.objects.filter(published = True)

news_item_view = NewsItemView.as_view()

class NewsRssFeed(Feed):
    title = "Jabber last news"
    link = "/rss/"
    description = "Jabber last news "
    
    def items(self):
        return News.objects.all()

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.body

class NewsSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.3
    
    def items(self):
        return News.objects.filter(published = True)

    def lastmod(self, obj):
        return obj.updated_datetime
